/**
 * define key
 */
function	Input()
{
    this.left = 37;
    this.right = 39;
    this.up = 38;
    this.down = 40;
    this.space = 32;
    this.shift = 16;
    this.ctrl = 17;
    this.x = 88;
    this.c = 67;
    this.reorianteRight = this.x;
    this.reorianteLeft = this.c;
}
