var patern2 = new Patern;
paterns["patern2"] = patern2;

//Vague VI
patern2.addMob(0, ship_testing, verticalLine, 10);
patern2.addMob(0, ship_testing, verticalLine, 70);
patern2.addMob(0, ship_testing, verticalLine, 120);
patern2.addMob(0, ship_testing, verticalLine, 170);
patern2.addMob(0, ship_testing, verticalLine, 220);
patern2.addMob(0, ship_testing, verticalLine, 280);

patern2.addMob(3, ship_testing, verticalLine, 10);
patern2.addMob(3, ship_testing, verticalLine, 73);
patern2.addMob(3, ship_testing, verticalLine, 120);
patern2.addMob(3, ship_testing, verticalLine, 173);
patern2.addMob(3, ship_testing, verticalLine, 220);
patern2.addMob(3, ship_testing, verticalLine, 280);

patern2.addMob(6, ship_testing, verticalLine, 10);
patern2.addMob(6, ship_testing, verticalLine, 76);
patern2.addMob(6, ship_testing, verticalLine, 120);
patern2.addMob(6, ship_testing, verticalLine, 176);
patern2.addMob(6, ship_testing, verticalLine, 220);
patern2.addMob(6, ship_testing, verticalLine, 280);

patern2.addMob(9, ship_testing, verticalLine, 10);
patern2.addMob(9, ship_testing, verticalLine, 79);
patern2.addMob(9, ship_testing, verticalLine, 120);
patern2.addMob(9, ship_testing, verticalLine, 179);
patern2.addMob(9, ship_testing, verticalLine, 220);
patern2.addMob(9, ship_testing, verticalLine, 280);
