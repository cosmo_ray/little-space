
var patern1 = new Patern;
paterns["patern1"] = patern1;

//Vague I
patern1.addMob(0, ship_testing, verticalLine, 70);
patern1.addMob(0, ship_testing, verticalLine, 120);
patern1.addMob(0, ship_testing, verticalLine, 170);
patern1.addMob(0, ship_testing, verticalLine, 220);
//Vague II
patern1.addMob(10, fast_ship_testing, slow_zigzag, 100);
patern1.addMob(10, fast_ship_testing, slow_zigzag, 200);
patern1.addMob(10, ship_testing, zigzag, 100);
patern1.addMob(10, ship_testing, zigzag, 200);
//Vague III
patern1.addMob(20, ship_testing, verticalLine, 70);
patern1.addMob(20, ship_testing, verticalLine, 120);
patern1.addMob(20, ship_testing, verticalLine, 170);
patern1.addMob(20, ship_testing, verticalLine, 220);
//Vague IV
patern1.addMob(40, ship_testing, verticalLine, 70);
patern1.addMob(40, ship_testing, verticalLine, 120);
patern1.addMob(40, ship_testing, verticalLine, 170);
patern1.addMob(40, ship_testing, verticalLine, 220);
